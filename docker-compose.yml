version: '3'

# Note: Some user content is in the user_data folder and some is in the Docker volumes.

services:
  import-automator:
    container_name: pwndoc-import-automator
    build: .
    restart: unless-stopped
    extra_hosts:
      - "host.docker.internal:host-gateway"
    environment:
      - IN_DOCKER=True
    env_file:
      - docker.env
    volumes:
      - ./user_data/pwndoc-init:/app/user_data/pwndoc-init
      - ./user_data/importer-logs:/app/user_data/importer-logs
      - ./user_data/importer-db:/app/user_data/importer-db
      - ./user_data/pwndoc-logs:/app/user_data/pwndoc-logs:ro  # Flask web interface allows download of Pwndoc Backend log
      - ./user_data/worker-logs:/app/user_data/worker-logs:ro
      # - ./scan2report:/app/scan2report  # modified docx templates and plugin configurations should be uploaded through web UI. Their temp representation may be in ./debug_tmp/scan2report_data
      - ./debug_tmp:/app/debug_tmp  # This is no longer just a debug folder, now it sadly also contains some persistent content (open ports for usage in audits)
    depends_on:
      - pwndoc-backend
      - importer-redis
    links:
      - pwndoc-backend
      - importer-redis
    networks:
      - backend

  import-automator-worker:
    container_name: pwndoc-import-automator-worker
    build: .
    restart: unless-stopped
    environment:
      - IN_DOCKER=True
    env_file:
      - docker.env
    volumes:
      - ./user_data/pwndoc-init:/app/user_data/pwndoc-init
      - ./user_data/worker-logs:/app/user_data/importer-logs
      - ./user_data/importer-db:/app/user_data/importer-db
      - ./scan2report:/app/scan2report  # preserves modified docx templates and plugin configurations; scan2report templates are preserved in pwndoc
      - ./debug_tmp:/app/debug_tmp
    depends_on:
      - pwndoc-backend
      - importer-redis
    links:
      - pwndoc-backend
      - importer-redis
    networks:
      - backend
    entrypoint: ["/bin/sh", "-c", "/usr/local/bin/rq worker --path /app --url redis://importer-redis 2>&1 | tee -a /app/user_data/importer-logs/worker-full.log"]

  pwndoc-mongo:
    # image: mongo:4.2.15    # This is PwnDoc native.
    image: mongo:4.4.17  # This would enable support for allowDiskUse during sorts over 100MB.
    container_name: pwndoc-mongo # note: This name is the reverse of original name (pwndoc-mongo vs mongo-pwndoc)
    volumes:
      - mongo-data:/data/db  # on Windows you can't use mapping to folder - there Mongo needs a proper volume
    restart: unless-stopped
    environment:
      - MONGO_DB:pwndoc
    networks:
      - backend


  pwndoc-backend:
    build: ./pwndoc-ng/backend
    container_name: pwndoc-backend
    volumes:
      - ./user_data/pwndoc-docx-templates:/app/report-templates
      - ./user_data/pwndoc-config:/app/src/config
      - ./user_data/pwndoc-logs:/app/custom_logs
    depends_on:
      - pwndoc-mongo
    restart: unless-stopped
    links:
      - pwndoc-mongo
    networks:
      - backend
    # entrypoint: ["npm", "start"] # this is default, but I want a copy of the logs to a file 
    entrypoint: ["/bin/sh", "-c", "npm start 2>&1| tee -a /app/custom_logs/pwndoc.log"]

  pwndoc-frontend:
    build: ./pwndoc-ng/frontend
    container_name: pwndoc-frontend
    restart: unless-stopped
    depends_on:
      - pwndoc-backend
    networks:
      - backend

  importer-redis:
    container_name: importer-redis
    image: redis
    restart: unless-stopped
    networks:
      - backend

  gateway:
    container_name: pwndoc-system-gateway
    build: ./nginx
    depends_on:
      - import-automator
      - pwndoc-frontend
    ports:
      - "8443:443"
    networks:
      - backend

volumes:
  mongo-data:

networks:
  backend:
      driver: bridge
