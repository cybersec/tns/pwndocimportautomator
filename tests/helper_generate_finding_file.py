import random
from typing import List


def generate_text_for_issues(partials_fids: List[str]) -> str:
    start = _get_start()
    end = _get_end()
    findings: List[str] = [single_issue(partial_fid) for partial_fid in partials_fids]

    return start + "\n\n".join(findings) + end


def _get_start() -> str:
    return """<?xml version="1.0"?>
<!DOCTYPE issues [
<!ELEMENT issues (issue*)>
<!ATTLIST issues burpVersion CDATA "">
<!ATTLIST issues exportTime CDATA "">
<!ELEMENT issue (serialNumber, type, name, host, path, location, severity, confidence, issueBackground?, remediationBackground?, issueDetail?, issueDetailItems?, remediationDetail?, requestresponse*)>
<!ELEMENT serialNumber (#PCDATA)>
<!ELEMENT type (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT host (#PCDATA)>
<!ATTLIST host ip CDATA "">
<!ELEMENT path (#PCDATA)>
<!ELEMENT location (#PCDATA)>
<!ELEMENT severity (#PCDATA)>
<!ELEMENT confidence (#PCDATA)>
<!ELEMENT issueBackground (#PCDATA)>
<!ELEMENT remediationBackground (#PCDATA)>
<!ELEMENT issueDetail (#PCDATA)>
<!ELEMENT issueDetailItems (issueDetailItem*)>
<!ELEMENT issueDetailItem (#PCDATA)>
<!ELEMENT remediationDetail (#PCDATA)>
<!ELEMENT requestresponse (request?, response?, responseRedirected?)>
<!ELEMENT request (#PCDATA)>
<!ATTLIST request method CDATA "">
<!ATTLIST request base64 (true|false) "false">
<!ELEMENT response (#PCDATA)>
<!ATTLIST response base64 (true|false) "false">
<!ELEMENT responseRedirected (#PCDATA)>
]>
<issues burpVersion="1.6.05" exportTime="Sat Sep 13 22:39:44 CEST 2014">

    """


def _get_end() -> str:
    return """
</issues>
"""


def single_issue(partial_fid):
    return f"""
  <issue>
    <serialNumber>{random.randint(10000, 1000000000)}</serialNumber>
    <type>{partial_fid}</type>
    <name>Example {partial_fid}</name>
    <host ip="127.0.0.1">http://example.com</host>
    <path>EXAMPLE_PATH</path>
    <location>EXAMPLE</location>
    <severity>High</severity>
    <confidence>Firm</confidence>
    <issueBackground>issueBackground</issueBackground>
    <remediationBackground>remediationBackground</remediationBackground>
    <issueDetail>issueDetail</issueDetail>
    <remediationDetail>remediationDetail</remediationDetail>
    <requestresponse>
      <request method="POST" base64="true"></request>
      <response base64="true"></response>
      <responseRedirected>false</responseRedirected>
    </requestresponse>
  </issue>
"""
