from typing import List, Optional, Callable, Any, Tuple, Dict
import pytest
from pytest_steps import test_steps
from copy import deepcopy

from helpers.file_root import relative_path

from tests.conftest import TMP_PATH, PWNDOC_DANGER_OVERRIDE
from tests.test_findings_parsing import test_connectivity, PwnDocCommunication
from tests.test_finding_grouping import FindingHelper

import config
from loguru import logger


@pytest.mark.skipif(not PWNDOC_DANGER_OVERRIDE, reason="PWNDOC_DANGER_OVERRIDE must be allowed to enable testing of interaction with PwnDoc")
@pytest.mark.dependency(depends=["test_connectivity"])
class TestAuditExports:

    @staticmethod
    def generic_finding_export(export_format: str = 'csv'):
        folder_name = PwnDocCommunication.setup_fake_scan2report_import(
            'tests/test_files/scan2report_results/single-finding-result',
            2, FindingHelper.finding_modification_set_fids, {'fids': ["burp_importer_test_fid1", "burp_importer_test_fid2"]}
        )
        resp = PwnDocCommunication.download_csv_or_json_findings(folder_name, export_format)
        assert resp.status_code == 200

    def test_json_finding_export(self, client):
        self.generic_finding_export('json')

    def test_csv_finding_export(self, client):
        self.generic_finding_export('csv')
