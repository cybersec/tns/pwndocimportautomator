import api_process_findings
from app import create_app, get_pre_initialized_app

with get_pre_initialized_app().test_request_context('/make_report/2017', data={'format': 'short'}):
    api_process_findings.upload_scan_findings_to_pwndoc('2022-09-22T12-57-38_findings_import_4b7d008f')
