from typing import List, Optional
import pytest


def test_request_example(client):
    response = client.get("/", follow_redirects=True)
    assert response.status_code == 200


