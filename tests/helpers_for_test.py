import requests
import config
import pwndoc_api


def pwndoc_connectivity_test():
    ok = True
    try:
        resp = pwndoc_api.session.get(f'{config.PWNDOC_URL}/api/users/init')  # This is safe noop.
        assert resp.status_code == 200
    except requests.exceptions.ConnectionError:
        ok = False  # assert is not here to make the fail output shorter and more readable
    assert ok, "PwnDoc is not reachable. Is it running?"
