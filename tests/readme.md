

```bash
cd pwndocImportAutomator # go to the root of repository

# setup docker.env and .env files

docker-compose -f docker-compose.yml -f docker-compose.dev-override.yml up --build -d

# Allow tests to overwrite the PwnDoc DB
export PWNDOC_DANGER_OVERRIDE=True

coverage run -m pytest -m "not veryslow"
```
