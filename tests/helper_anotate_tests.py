from typing import List, Optional, Any
import pytest


def skip_and_anotate_problematic_test_files(filepaths: List[str]) -> List[str]:
    for i in range(len(filepaths)):
        problematic_reason = _reasons_for_problematic_test_files(filepaths[i])
        if problematic_reason:
            filepaths[i] = pytest.param(filepaths[i], marks=pytest.mark.skip(reason=problematic_reason))
    return filepaths


def skip_confidential(filepaths: List[Any]) -> List[str]:  # todo: this may be Union[str, ParameterSet]
    for i in range(len(filepaths)):
        filepath = filepaths[i]
        if skip_confidential and isinstance(filepath, str) and "confidential" in filepath.lower():
            filepaths[i] = pytest.param(filepaths[i], marks=pytest.mark.skip(reason="Skipping confidential tests."))
    return filepaths


def _reasons_for_problematic_test_files(filepath: str) -> Optional[str]:
    no_parsing_by_scan2report = "Old nessus formats are not supported by scan2report"

    skip_problematic_with_reason = {
        'example_v1.nessus': no_parsing_by_scan2report,
        'example_v2.nessus': no_parsing_by_scan2report,
        'example_v_wrong.nessus': no_parsing_by_scan2report,
        'nessus_report_test_local.v1.nessus': no_parsing_by_scan2report,
    }

    for problematic_name, reason in skip_problematic_with_reason.items():
        if problematic_name in filepath:
            return reason
    return None


