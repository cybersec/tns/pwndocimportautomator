#!/bin/bash

set -e

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo $parent_path
cd $parent_path

folderName=backups/$(date +"%Y-%m-%dT%H-%M-%S")
echo $folderName
mkdir $folderName
docker exec pwndoc-mongo sh -c 'exec mongodump --archive' > "$folderName/mongo-db.backup"
zip -r $folderName/user_data.zip user_data/
zip -r $folderName/debug_tmp.zip debug_tmp/
zip -r $folderName/misc.zip nginx/ .env docker.env 

echo "Backup saved to folder ${folderName}"
