import html

from flask import render_template, has_request_context, Response


def basic_msg(content: str):
    return basic_msg_html(content, is_content_safe=False)


def basic_msg_html(content_html: str, is_content_safe: bool = True):
    if not is_content_safe:
        content_html = html.escape(content_html)

    if not has_request_context():
        return content_html

    return render_template('basic_msg.html', contentHTML=content_html)


def response_as_download(text: str, filename: str, file_format: str = 'json') -> Response:
    mimetypes = {
        'csv': 'text/csv',
        'json': 'application/json',
    }
    return Response(
        text,
        mimetype=mimetypes.get(file_format),
        headers={"Content-disposition": f"attachment; filename={filename}"}
    )
