from time import time
from datetime import datetime


def current_timestamp() -> int:
    return int(time())


def timestamp_to_iso(timestamp) -> str:
    return datetime.fromtimestamp(timestamp).isoformat()
