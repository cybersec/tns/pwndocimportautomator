import os


def relative_path(relative_filepath: str) -> str:
    joined_path = os.path.join(os.path.dirname(__file__), "..", relative_filepath)
    normalized_path = os.path.normpath(joined_path)
    return normalized_path
