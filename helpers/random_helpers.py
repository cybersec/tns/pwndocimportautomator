import random
import string


def random_str(n: int = 8, alphabet: str = string.ascii_letters) -> str:
    return ''.join(random.choice(alphabet) for _ in range(n))

