from dotenv import load_dotenv
import os
from loguru import logger
from helpers.file_root import relative_path

SCAN2REPORT_ORIGINAL_FOLDER = relative_path("scan2report")
SCAN2REPORT_ALTERNATIVE_FOLDER = relative_path("debug_tmp/scan2report_data")
SCAN2REPORT_PLUGINS_FOLDER = os.path.join(SCAN2REPORT_ALTERNATIVE_FOLDER, 'plugins')
LOCALES = ["cs", "en", "og"]
assert LOCALES[-1] == "og", "inner logic of scan2report parsing depends on 'og' being the last locale"

load_dotenv()

SECRET_KEY = os.environ.get('FLASK_SECRET_KEY')

ALLOWED_SCANNER_EXTENSIONS = {'dat', 'xml', 'nessus', 'json'}

FLASK_UPLOAD_FOLDER = relative_path('debug_tmp')

IMPORTER_URL_PREFIX = '/import_automator'  # Prefix is for situation when PwnDoc and Importer share domain and port behind the reverse proxy.

DB_URI = 'sqlite:///user_data/importer-db/main.db'

PWNDOC_URL = os.getenv('PWNDOC_URL').rstrip("/")
PWNDOC_USERNAME = os.getenv('PWNDOC_USERNAME')
PWNDOC_PASSWORD = os.getenv('PWNDOC_PASSWORD')
PWNDOC_DISABLE_HTTPS_VERIFICATION = os.getenv("PWNDOC_DISABLE_HTTPS_VERIFICATION", False) == "True"

PWNDOC_REQUEST_SIZE = 10*1000*1000

if PWNDOC_DISABLE_HTTPS_VERIFICATION:
    logger.warning(f"Security warning: HTTPS certificate validation for requests is DISABLED.")

LOGGER_INTERCEPT_STD_LOGGING = os.getenv('LOGGER_INTERCEPT_STD_LOGGING', False) == "True"

TNS_DESCRIPTION_PROOF_DELIMITER = "========================================"

IN_DOCKER = os.getenv("IN_DOCKER", False) == "True"
BEHIND_REVERSE_PROXY = IN_DOCKER

RQ_DISABLED = os.getenv("RQ_DISABLED", False) == "True"
REDIS_HOST = 'importer-redis' if IN_DOCKER else 'localhost'
REDIS_PORT = 6379 if IN_DOCKER else 5002
RQ_JOB_TIMEOUT = int(os.getenv("RQ_JOB_TIMEOUT", 30*60))  # seconds

# These feature controls have safe defaults and exist only for tests to modify them.
PWNDOC_NO_CONNECTION_PLACEHOLDER = 'https://404.local'
APP_INIT_PWNDOC_DISALLOW_WAITING = False

