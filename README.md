# pwndocImportAutomator

Are you a multilingual small team of pentesters, who would like to collaborate on pentest reports via web browser, but would also like to have creature comforts like import of findings from vulnerability scanning tools? If so, this tool is for you.

## Intended workflow

1. Perform the vulnerability/policy compliance scans using the supported tools (Nessus, Burp, Lynis, Kubeaudit, CHAPS).
2. Create an audit in [PwnDoc](https://github.com/pwndoc/pwndoc) (see it's repo for great screenshots)
3. In the web UI of the Importer Automator select the audit and upload the results from the vulnerability scanning tools.
4. Go to PwnDoc and work on the audit.
5. When the audit is ready, download the Word document either directly in PwnDoc, or if its generation is too slow then use the Import Automator.
6. Finally, go through the findings in the audit and if any of them did not have a previously prepared vulnerability template, propose the template creation based on that finding. This is a single-click process in PwnDoc.


## Deployment - Docker-compose

### Quickstart (unsafe, use only for testing)

The two following snippets are good enough for evaluation whether this project can help you. If you decide that you want to use it more long term, follow the instructions at the end of this section and in [Wiki](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/wikis/home) for a more thorough setup.

#### Installing Docker and Docker Compose

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

sudo curl -L "https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-$(uname -s)-$(uname -m)"  -o /usr/local/bin/docker-compose
sudo mv /usr/local/bin/docker-compose /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```

#### Unsafe quickstart

First run of `docker-compose up` can take several minutes (7.5 min in our tests) all further runs use caching and happen under 30 seconds.

```sh
git clone --recurse-submodules https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator.git
cd pwndocimportautomator

touch nginx/trusted_client_certs.pem
cp docker.env.dist docker.env
docker-compose up --build -d
# https://IP:8443/
# username FILL_IN
# password FillMeIn42
```

Public vulnerability templates are automatically imported to PwnDoc. For testing of the import from vulnerability scans you can you one of the [nessus test files](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/blob/main/tests/test_files/scanner_results/DefectDojo/nessus/nessus_with_cvssv3.nessus).

### Web interface

HTTPS on port 8443
- Pwndoc: `/`
- ImportAutomator: `/import_automator` 

### Correct setup

```sh

git clone --recurse-submodules https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator.git
cd pwndocimportautomator

# MANUAL STEP: Fill in docker.env file (use docker.env.dist as a template)

# Setup authentication
# Option 1: Keep it disabled (this gives access to audit data to EVERYONE)
    # touch nginx/trusted_client_certs.pem
# Option 2: Set up TLS Client Auth
    # MANUAL STEP: create file nginx/trusted_client_certs.pem and put certificate(s) (CA or individual) there
    # sed -i 's/ssl_verify_client off;/ssl_verify_client on;/' nginx/nginx.conf
# Option 3: Use password based authentication (see file nginx/nginx.conf)
# Option 4: Authenticate using another (external) reverse proxy. In that case make sure support for websockets is enabled.

docker-compose up --build -d
```

For more advanced customization see [Wiki](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/wikis/setup).

## Stopping containers

```sh
docker-compose down --remove-orphans
# note: flag -v would also remove the docker volume with the PwnDoc database (DANGEROUS)
```

## Minimum requirements

2 GB RAM, 1 CPU core, 15 GB (VM including Ubuntu 22.04 and Docker took 6.3 GB)

Never run with less RAM - MongoDB can panic and start flushing everything right away, resulting in a constant write/read to disk. We've seen constant 140 MB/s stream of (re)writes during import of findings which would quickly destroy a SSD.

## How to backup

Before first backup:
`sudo apt update && sudo apt install zip`

To run backup:
`./backup.sh`

## How to update

Some [releases](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/releases) may include new functionality, that needs to be setup manually. Those are listed with the relevant instructions. All updates 

```sh
./backup.sh
git pull --recurse-submodules -X ours
./backup.sh
docker-compose up --build -d
```

#### Version 2022-09-29

This version needs a manual step - TLS Client Auth. See the first-run section above.

#### Version 2022-10-27

This version introduces more strict PwnDoc password requirements, so it might require change of password and related modification of files `.env` and `docker.env`.
- _Password must be at least 8 characters with minimum 1 Uppercase, Lowercase and Number_

#### Version 2022-12-01

Scan2Report repository now contains publicly released set of templates and plugin configuration files. These might colide with your existing templates in the same location. The following command can solve it.

```sh
mkdir -p backups/2022-12-01-release
zip -r backups/2022-12-01-release/scan2report_plugins.zip scan2report/plugins/

mkdir -p debug_tmp/scan2report_data
cp -r scan2report/plugins/* debug_tmp/scan2report_data

# https://stackoverflow.com/questions/15404535/how-to-git-reset-hard-a-subdirectory/15404733#15404733
cd scan2report
rm -rf plugins/*
git restore --source=HEAD --staged --worktree -- plugins/

cd ..
git pull --recurse-submodules -X ours
```

This will not be problem in future - from this release onward the Importer will save its temporary data into `debug_tmp\scan2report_data\`.


#### Version 2023-02-05

This version switches from [PwnDoc](https://github.com/pwndoc/pwndoc) (which is most likely no longer maintained) to [PwnDoc-ng](https://github.com/pwndoc-ng/pwndoc-ng) (a community fork).

The submodule `pwndoc` remains as an unused dependency, so that the update is guaranteed to be non-destructive and reversible if need be.

New submodule `pwndoc-ng` points to a [customized version of Pwndoc-ng](https://github.com/BorysekOndrej/pwndoc-ng/commits/import-automator-stable). In future when rebasing it beware that pwndoc-ng does not guarantee that master is buildable, but its releases should be.

No special update process is needed for this update, though you might encounter a merge conflict with `docker-compose.yml` if you've modified it.

## License

This project is licensed under MIT as are its dependencies PwnDoc and Scan2Report.

The exception are:

- files in folder `tests/test_files/scanner_results\` which are sourced from other projects available on GitHub. The [links to their sources](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/blob/main/tests/test_files/scanner_results/test_files.txt) and the licenses are attached in the [respective folders](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/blob/main/tests/test_files/scanner_results/).
- files in folders `static/jdd` and `static/npm` which are local copies of available libraries. They have their own licenses.


## Documentation

Further documentation is on the [Wiki](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/wikis/home).
