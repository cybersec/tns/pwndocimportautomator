The CA and Client certs can be created for example using the following tutorial.
https://www.makethenmakeinstall.com/2014/05/ssl-client-authentication-step-by-step/

Be sure to set different Common names during cert generation - otherwise the certs look self signed in openssl.
https://stackoverflow.com/questions/19726138/openssl-error-18-at-0-depth-lookupself-signed-certificate
