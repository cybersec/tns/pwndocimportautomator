
function audit_specific_event(elem){
    let relative_url = elem.getAttribute("url");
    let audit_id = document.getElementById('audit').value;

    relative_url = relative_url.replace("AUDIT_ID_PLACEHOLDER", audit_id);
    let absolute_url = new URL(relative_url, document.baseURI).href;
    window.location.href = absolute_url;
}
