FROM python:3.8

WORKDIR /app

# SETUP Scan2Report - BEGIN

RUN apt-get clean && apt-get update && apt-get install -y locales locales-all
RUN locale-gen cs_CZ && locale-gen cs_CZ.UTF-8 && update-locale cs_CZ.UTF-8
ENV LANG="cs_CZ.UTF-8" LC_ALL="cs_CZ.UTF-8" LC_CTYPE="cs_CZ.UTF-8"

# SETUP Scan2Report - END

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

# todo: lower the timeout back to 30 seconds when the scan2report processing is done asynchronously
# 90 seconds is timeout in Firefox

ENV FLASK_APP app.py
CMD [ "gunicorn", \
    "--bind", "0.0.0.0:5000", \
    "-w", "4", \
    "--worker-class", "gevent", \
    "--timeout", "85", \
    "--access-logfile", "user_data/importer-logs/gunicorn_access.log", \
    "--error-logfile", "user_data/importer-logs/gunicorn_error.log", \
    "app:create_app()"]
